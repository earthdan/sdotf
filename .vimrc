set nocompatible              " be iMproved, required
syntax on

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

Plug 'bling/vim-bufferline'
Plug 'mbbill/undotree'

Plug 'junegunn/vim-easy-align'

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
Plug 'chr4/nginx.vim', { 'for' : 'nginx' }
Plug 'tpope/vim-surround'
Plug 'easymotion/vim-easymotion'
Plug 'bling/vim-airline'
Plug 'vim-ctrlspace/vim-ctrlspace'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'mhartington/oceanic-next'
Plug 'jeetsukumaran/vim-indentwise'
Plug 'tpope/vim-fugitive'

" Initialize plugin system
call plug#end()

filetype plugin indent on    " required

if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

if !exists("g:ycm_semantic_triggers")
  let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers['typescript'] = ['.']

set display+=lastline

if &t_Co == 8 && $TERM !~# '^linux\|^Eterm'
  set t_Co=16
endif

set encoding=utf-8
set t_Co=256

colorscheme OceanicNext
let g:airline_theme='oceanicnext'

set relativenumber
set cursorline
set hidden
set showmode

set wildmenu
set laststatus=2
set autoindent
set backspace=indent,eol,start
set complete-=i

set nrformats-=octal


set tabstop=2
set shiftwidth=2
set expandtab

command! -bang Q q<bang>
command! -bang -nargs=* -complete=file E e<bang> <args>
command! -bang -nargs=* -complete=file W w<bang> <args>
command! -bang -nargs=* -complete=file Wq wq<bang> <args>
nnoremap <silent> <leader>tt :TagbarToggle<CR>

"Comment style for vim-commentary
autocmd FileType apache setlocal commentstring=#\ %s

if has("persistent_undo")
    set undodir=~/.undodir/
    set undofile
endif




nmap <leader>x :set relativenumber!<CR>

noremap <leader>tn :tabnext<CR>
noremap <leader>tp :tabprevious<CR>
noremap <leader>to :tabnew<CR>
noremap <leader>n :NERDTreeToggle<CR>
noremap <leader>w :w<CR>
noremap <leader>q :wq<CR>
nnoremap <Leader>o :CtrlP<CR>
nnoremap <Leader>b :CtrlSpace<CR>

noremap <Tab> :bprev<CR> 
noremap <S-Tab> :bnext<CR> 

nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>

